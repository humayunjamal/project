#!/usr/bin/python


import psycopg2 as postdb  # Import MySQL Libraimport sys, tracebackry
import pymongo as mondb  # Import MongoDB Library
import multiprocessing as mp  # Import Multiprocessing
from psycopg2 import extras
import json

# PostGreSQL Config
myConfig = {'host': 'pgmaster',
            'username': 'postgres',
            'password': 'password',
            'db': 'project'}


moncon = mondb.MongoClient('mongodb://mongouser:password@mongohost:27017/project')
mondb = moncon.project.api



# Fetch records from postgreSQL
def fetchproducts():
    mycon = postdb.connect(host=myConfig['host'], user=myConfig['username'],
                         password=myConfig['password'], dbname=myConfig['db'])
    cursor = mycon.cursor(cursor_factory=postdb.extras.RealDictCursor)
    SELECT_SQL = ("SELECT * FROM api")
    cursor.execute(SELECT_SQL)
    rows = cursor.fetchall()
    return [row for row in rows]
    #with mycon:

    #    cur = mycon.cursor()
    #    cur.execute("""SELECT * from api""")
    #    records = cur.fetchall()
    #    return records


#def write_to_mongo(list_of_dicts):


#    json_data = json.dumps(list_of_dicts)
#    print json_data

#    result = mondb.insert_many(list_of_dicts)

#    print dir(result)

#    print result.inserted_ids

def write_to_mongo(record):
    recordExist = mondb.find_one({'lname': record['lname']})

    if recordExist is None:
        result = mondb.insert_one(record)
        print result.inserted_id
        print "Objects created in MongoDB"
    else:
        print 'Record: ' + str(record['lname'])+' already exists'

def iterate_mongodb():
    print "Iterating All existing Records in MongoDB"
    results = mondb.find()
    print()
    print('+-+-+-+-+-+-+-+-+-+-+-+-+-+-')

# display documents from collection
    for record in results:
# print out the document
        print "AGE: %d  FIRST_NAME: %s   SECOND_NAM: %s" %(record['age'], record['fname'], record['lname'])

        print ()

# close the connection to MongoDB
    moncon.close()







# Main Function
def main():
    list_of_dicts = fetchproducts()

    for record in list_of_dicts:
        write_to_mongo(record)
    iterate_mongodb()


    #write_to_mongo(list_of_dicts)

    #for record in records:
    #    index(record)


# If you do not want MultiThreading uncomment the following line and comment
# the call of main function
# products = fetchproducts()

# for product in products:
#     index(product)

main()
